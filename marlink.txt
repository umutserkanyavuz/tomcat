http://192.168.0.1/dashboard

{
	"show_user_level": 1,
	"forwarding_wan": 1,
	"signal_strength": "3.0 dBHz",
	"call_type": 0,
	"wan_to_use": 1,
	"password_enabled": false,
	"pos_longitude": "E1000°00'",
	"answer_time": "0:00:00",
	"beam_type": "Global",
	"bgan_status": "Scanning",
	"user_level": "administrator",
	"pos_fix": 0,
	"local_ip": "192.168.0.1",
	"show_gnss_info": 1,
	"connection": [],
	"call_receiver": "-",
	"pos_satellites": 0,
	"pos_origin": 0,
	"call_originator": "-",
	"call_status": 0,
	"current_satellite": "None",
	"airtime_provider": "MARLINK AS",
	"has_pos_fix": 0,
	"pos_latitude": "N1000°00'"
}

http://192.168.0.1/battery

{
	"rows": [
		{
			"cols": [
				{
					"val": "Internal battery",
					"type": "header"
				}
			]
		},
		{
			"cols": [
				{
					"val": "Status",
					"type": "title"
				},
				{
					"val": "Charging",
					"type": "text",
					"id": ""
				}
			]
		},
		{
			"cols": [
				{
					"val": "Charge level",
					"type": "title"
				},
				{
					"val": "27 %",
					"type": "text",
					"id": ""
				}
			]
		},
		{
			"cols": [
				{
					"val": "Estimated remaining charge time",
					"type": "title"
				},
				{
					"val": "2h 25m",
					"type": "text",
					"id": ""
				}
			]
		},
		{
			"cols": [
				{
					"val": "Temperature",
					"type": "title"
				},
				{
					"val": "29 °C",
					"type": "text",
					"id": ""
				}
			]
		},
		{
			"cols": [
				{
					"val": "External battery",
					"type": "header"
				}
			]
		},
		{
			"cols": [
				{
					"val": "Status",
					"type": "title"
				},
				{
					"val": "No battery",
					"type": "text",
					"id": ""
				}
			]
		}
	]
}

http://192.168.0.1/header

{
	"battery_level": 27,
	"close_system_status_popups": 1,
	"wlan_enabled": 0,
	"popup": {
		"opcode": [
			101,
			103,
			104
		],
		"msg": 0
	},
	"wan_to_use": 1,
	"signal_strength_bgan_norm": 0,
	"signal_strength_modem_norm": -1,
	"show_wlan": 1,
	"alert_count": 0,
	"red_label_sw": 0,
	"battery_count": 1,
	"show_battery": 1,
	"link_network_popup": 0,
	"wlan_number_of_clients": 0,
	"battery_charging": 1
}